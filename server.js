var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

//línea para que use los componentes de /build/default
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Esperando que funciona Polymer con node.js: ' + port);

app.get('/', function(req, res) {
  res.sendFile("index.html", {root:'.'});
});
